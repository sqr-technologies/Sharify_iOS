//
//  AuthorizeResult.swift
//  Sharify
//
//  Created by Maximilian Seiferth on 26.12.18.
//  Copyright © 2018 Maximilian Seiferth. All rights reserved.
//

struct AuthorizeResult: Decodable {
    let state: String
    let client_id: String
    let scope: String
    let redirect_uri: String
    let response_type: String
}

struct TokenResult: Decodable {
    let access_token: String
    let refresh_token: String
}
