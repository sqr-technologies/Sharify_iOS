//
//  QueueResults.swift
//  Sharify
//
//  Created by Maximilian Seiferth on 30.12.18.
//  Copyright © 2018 Maximilian Seiferth. All rights reserved.
//

struct New_Queue_Result: Codable {
    let invite_token: String
}

struct Join_Queue_Result: Codable {
    let queue: Queue
}

struct QueueInfoResult: Codable {
    var queue: Queue
}

struct Queue: Codable {
    let id: Int
    let displayName: String
    let owner: String
    let members: [String]
    let elements: [QueueElement]
    var queueTracks: [TrackItem]?
}

struct QueueElement: Codable {
    let id: Int
    let spotifyID: String
}

struct QueueAdvanceResult: Codable {
    let queue: Queue
    let nextSongID: String
}
