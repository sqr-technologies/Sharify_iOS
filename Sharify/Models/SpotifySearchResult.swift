//
//  SpotifySearchResult.swift
//  Sharify
//
//  Created by Maximilian Seiferth on 27.11.19.
//  Copyright © 2019 Maximilian Seiferth. All rights reserved.
//

struct SearchResult: Codable {
    let albums: AlbumResult?
    let artists: ArtistResult?
    let tracks: TrackResult?
}

struct UserTopTracksResult: Codable {
    let items: [TrackItem]
}

struct SeveralTracksResult: Codable {
    let tracks: [TrackItem]
}

struct TrackResult: Codable {
    let href: String
    let items: [TrackItem]
    let limit: Int
    let next: String?
    let offset: Int
    let previous: String?
    let total: Int
}

struct AlbumResult: Codable {
    let href: String
    let items: [AlbumItem]
    let limit: Int
    let next: String?
    let offset: Int
    let previous: String?
    let total: Int
}

struct ArtistResult: Codable {
    let href: String
    let items : [ArtistItem]
    let limit: Int
    let next: String?
    let previous: String?
    let offset: Int
    let total: Int
}

struct TrackItem: Codable {
    let album: AlbumItem
    let artists: [ArtistAlbumItem]
    let disc_number: Int
    let duration_ms: Int
    let explicit: Bool
    let external_urls: external_urls
    let href: String
    let id: String
    let is_local: Bool
    let is_playable: Bool?
    let name: String
    let popularity: Int
    let preview_url: String?
    let track_number: Int
    let type: String
    let uri: String
}

struct ArtistItem: Codable {
    let external_urls: external_urls
    let followers: followers
    let genres: [String]
    let href: String
    let id: String
    let images: [ImageItem]
    let name: String
    let popularity: Int
    let type: String
    let uri: String
}

struct followers: Codable {
    let href: String
    let total: Int
}

struct AlbumItem: Codable {
    let album_type: String
    let artists: [ArtistAlbumItem]
    let external_urls: external_urls
    let href: String
    let id: String
    let images: [ImageItem]
    let name: String
    let release_date: String
    let release_date_precision: String
    let total_tracks: Int
    let type: String
    let uri: String
}

struct ArtistAlbumItem: Codable {
    let external_urls: external_urls
    let href: String
    let id: String
    let name: String
    let type: String
    let uri: String
}

struct ImageItem: Codable {
    let height: Int
    let width: Int
    let url: String
}

struct external_urls: Codable {
    let spotify: String
}
