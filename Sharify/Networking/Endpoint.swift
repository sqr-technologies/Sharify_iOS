//
//  File.swift
//  Sharify
//
//  Created by Maximilian Seiferth on 26.12.18.
//  Copyright © 2018 Maximilian Seiferth. All rights reserved.
//

import Foundation

enum HttpMethod: String {
    case GET = "GET"
    case POST = "POST"
    case PUT = "PUT"
}

enum AuthType {
    case own
    case spotify
}

protocol Endpoint {
    var base: String { get }
    var path: String { get }
    var method: HttpMethod { get }
    var requiresAuthentication: Bool { get }
    var authType: AuthType { get }
}

extension Endpoint {
    var urlComponents: URLComponents {
        var components = URLComponents(string: base)!
        components.path = path
        //components.query = apikey
        return components
    }
    
    var request: URLRequest {
        let url = urlComponents.url!
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        if requiresAuthentication {
            //let access_token = UserDefaults.standard.string(forKey: "access_token")
            //let refresh_token = UserDefaults.standard.string(forKey: "refresh_token")
            
            /*var authToken = ""
            if authType == .spotify {
                authToken = "Bearer " + access_token!
            } else {
                authToken = refresh_token!
            }
            request.addValue(authToken, forHTTPHeaderField: "Authorization")*/
        }
        return request
    }
}

enum API {
    case auth_init
    case new_queue
    case join_queue
    case addTrack
    case current_queue
    case advance
}

extension API: Endpoint {
    var base: String {
        return "http://192.168.178.63:3000"
    }
    
    var path: String {
        switch self {
        case .auth_init: return "/api/v1/auth"
        case .new_queue: return "/api/v1/host_queue"
        case .join_queue: return "/api/v1/join_queue"
        case .addTrack: return "/api/v1/current_queue/append"
        case .current_queue: return "/api/v1/current_queue"
        case .advance: return "/api/v1/current_queue/advance"
        }
    }
    
    var method: HttpMethod {
        switch self {
        case .auth_init:
            return .POST
        case .new_queue:
            return .POST
        case .join_queue:
            return .PUT
        case .addTrack:
            return .POST
        case .current_queue:
            return .GET
        case .advance:
            return .POST
        }
    }
    
    var requiresAuthentication: Bool {
        switch self {
        case .auth_init: return false
        case .new_queue: return true
        case .join_queue: return true
        case .addTrack: return true
        case .current_queue: return true
        case .advance: return true
        }
    }
    
    var authType: AuthType {
        return .own
    }
}


enum SpotifyAPI {
    case search
    case userTopTracks
    case getSeveralTracks
}

extension SpotifyAPI: Endpoint {
    var base: String {
        return "https://api.spotify.com"
    }
    
    var path: String {
        switch self {
        case .search: return "/v1/search"
        case .userTopTracks: return "/v1/me/top/tracks"
        case .getSeveralTracks: return "/v1/tracks"
        }
    }
    
    var method: HttpMethod {
        switch self {
        case .search: return .GET
        case .userTopTracks: return .GET
        case .getSeveralTracks: return .GET
        }
    }
    
    var requiresAuthentication: Bool {
        switch self {
        case .search: return true
        case .userTopTracks: return true
        case .getSeveralTracks: return true
        }
    }
    
    var authType: AuthType {
        return .spotify
    }
    
}
