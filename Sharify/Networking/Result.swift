//
//  Result.swift
//  Sharify
//
//  Created by Maximilian Seiferth on 26.12.18.
//  Copyright © 2018 Maximilian Seiferth. All rights reserved.
//

enum Result<T, U> where U: Error {
    case success(T)
    case failure(U)
}

