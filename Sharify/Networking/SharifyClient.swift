//
//  SharifyClient.swift
//  Sharify
//
//  Created by Maximilian Seiferth on 26.12.18.
//  Copyright © 2018 Maximilian Seiferth. All rights reserved.
//
import Foundation
class SharifyClient: APIClient {
    let session: URLSession
    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    
    convenience init() {
        self.init(configuration: .default)
    }
    
    func authorize_init(completion: @escaping (Result<AuthorizeResult?, APIError>) -> Void) {
        let request = API.auth_init.request
        fetch(with: request, decode: {json -> AuthorizeResult? in
            guard let authorizeResult = json as? AuthorizeResult else { return nil }
            return authorizeResult
        }, completion: completion)
    }
    
    func createNewQueue(queueParams: Dictionary<String, String>, completion: @escaping (Result<New_Queue_Result?, APIError>) -> Void) {
        var request = API.new_queue.request
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: queueParams, options: .prettyPrinted)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue(SpotifySessionManager.shared.sessionManager.session?.refreshToken ?? "", forHTTPHeaderField: "Authorization")
            
            fetch(with: request, decode: {json -> New_Queue_Result? in
                guard let queueResult = json as? New_Queue_Result else { return nil }
                return queueResult
            }, completion: completion)
        }
        catch {
            print("Error: \(error.localizedDescription)")
            return
        }
       
    }
    
    func joinQueue(token: String, completion: @escaping (Result<Join_Queue_Result? ,APIError>) -> Void) {
        var request = API.join_queue.request
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: ["invite_token" : token], options: .prettyPrinted)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue(SpotifySessionManager.shared.sessionManager.session?.refreshToken ?? "", forHTTPHeaderField: "Authorization")
            
            fetch(with: request, decode: {json -> Join_Queue_Result? in
                guard let joinQueueResult = json as? Join_Queue_Result else { return nil }
                return joinQueueResult
            }, completion: completion)
        } catch {
            print("Error: ", error.localizedDescription)
            return
        }
    }
    
    func getQueueInfo(completion: @escaping (Result<QueueInfoResult?, APIError>) -> Void) {
        var request = API.current_queue.request
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(SpotifySessionManager.shared.sessionManager.session?.refreshToken ?? "", forHTTPHeaderField: "Authorization")
        
        fetch(with: request, decode: { json -> QueueInfoResult? in
            guard let queueInfo = json as? QueueInfoResult else { return nil }
            return queueInfo
        }, completion: completion)
    }
    
    func addTrack(track: TrackItem, completion: @escaping (Result<QueueInfoResult?, APIError>) -> Void) {
        var request = API.addTrack.request
        do {
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue(SpotifySessionManager.shared.sessionManager.session?.refreshToken ?? "", forHTTPHeaderField: "Authorization")
            request.httpBody = try JSONSerialization.data(withJSONObject: ["spotifyID" : track.id], options: .prettyPrinted)
            
            fetch(with: request, decode: { json in
                guard let queueInfo = json as? QueueInfoResult else { return nil }
                return queueInfo
            }, completion: completion)
        } catch {
            print(error.localizedDescription)
            return
        }
    }
    
    func advanceQueue(completion: @escaping (Result<QueueAdvanceResult?, APIError>) -> Void) {
        var request = API.advance.request
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(SpotifySessionManager.shared.sessionManager.session?.refreshToken ?? "", forHTTPHeaderField: "Authorization")
        
        fetch(with: request, decode: { json -> QueueAdvanceResult? in
            guard let queueAdvanceResult = json as? QueueAdvanceResult else { return nil }
            return queueAdvanceResult
        }, completion: completion)
    }
    
}
