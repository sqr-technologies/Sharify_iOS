//
//  SpotifyClient.swift
//  Sharify
//
//  Created by Maximilian Seiferth on 27.11.19.
//  Copyright © 2019 Maximilian Seiferth. All rights reserved.
//

import UIKit

class SpotifyClient: APIClient {
    let session: URLSession
    
    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    
    convenience init() {
        self.init(configuration: .default)
    }
    
    func search(query: String, offset: String, completion: @escaping (Result<SearchResult?, APIError>) -> Void) {
        if SpotifySessionManager.shared.sessionManager.session!.isExpired {
            SpotifySessionManager.shared.sessionManager.renewSession()
        }
        
        var request = SpotifyAPI.search.request
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer " + SpotifySessionManager.shared.sessionManager.session!.accessToken, forHTTPHeaderField: "Authorization")
        
        let queryItem = URLQueryItem(name: "q", value: query)
        let queryTypeItem = URLQueryItem(name: "type", value: "track")
        let queryLimitItem = URLQueryItem(name: "limit", value: "30")
        let queryOffsetItem = URLQueryItem(name: "offset", value: offset)
        
        print(query, offset)
        
        var url = URLComponents(url: request.url!, resolvingAgainstBaseURL: false)
        url?.queryItems = [queryItem, queryTypeItem, queryLimitItem, queryOffsetItem]
        
        request.url = url?.url
        
        fetch(with: request, decode: {json -> SearchResult? in
            guard let searchResult = json as? SearchResult else { return nil }
            return searchResult
        }, completion: completion)
        
    }
    
    func getUserTop(offset: String, completion: @escaping (Result<UserTopTracksResult?, APIError>) -> Void) {
        if SpotifySessionManager.shared.sessionManager.session!.isExpired {
            SpotifySessionManager.shared.sessionManager.renewSession()
        }
        
        var request = SpotifyAPI.userTopTracks.request
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer " + SpotifySessionManager.shared.sessionManager.session!.accessToken, forHTTPHeaderField: "Authorization")
        
        let queryLimitItem = URLQueryItem(name: "limit", value: "30")
        let queryOffsetItem = URLQueryItem(name: "offset", value: offset)
        let queryTimeFrame = URLQueryItem(name: "time_range", value: "short_term")
        
        var url = URLComponents(url: request.url!, resolvingAgainstBaseURL: false)
        url?.queryItems = [queryLimitItem, queryOffsetItem, queryTimeFrame]
        
        request.url = url?.url
        
        fetch(with: request, decode: {json -> UserTopTracksResult? in
            guard let UserTopTracksResult = json as? UserTopTracksResult else { return nil }
            return UserTopTracksResult
        }, completion: completion)
        
    }
    
    func getTracks(tracks: [String], completion: @escaping (Result<SeveralTracksResult?, APIError>) -> Void) {
        if SpotifySessionManager.shared.sessionManager.session!.isExpired {
            SpotifySessionManager.shared.sessionManager.renewSession()
        }
        
        var request = SpotifyAPI.getSeveralTracks.request
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer " + SpotifySessionManager.shared.sessionManager.session!.accessToken, forHTTPHeaderField: "Authorization")
        
        let idsItem = URLQueryItem(name: "ids", value: tracks.joined(separator: ","))
        
        var url = URLComponents(url: request.url!, resolvingAgainstBaseURL: false)
        url?.queryItems = [idsItem]
        
        request.url = url?.url
        
        fetch(with: request, decode: {json -> SeveralTracksResult? in
            guard let severalTracks = json as? SeveralTracksResult else { return nil }
            return severalTracks
        }, completion: completion)
        
    }
}
