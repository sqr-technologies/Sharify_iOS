//
//  SpotifySession.swift
//  Sharify
//
//  Created by Maximilian Seiferth on 25.11.19.
//  Copyright © 2019 Maximilian Seiferth. All rights reserved.
//

import UIKit

class SpotifySessionManager: NSObject {
    static let shared = SpotifySessionManager()
    
    var appRemote: SPTAppRemote?
    let tokenSwapLocal = "http://192.168.178.63:3000/api/v1/token_swap"
    let tokenRefreshLocal = "http://192.168.178.63:3000/api/v1/token_refresh"
    lazy var configuration: SPTConfiguration = {
        let config = SPTConfiguration(clientID: "222bda6cd33545c09cc94d6f00b8aaec", redirectURL: URL(string: "sharify://auth_callback")!)
        config.tokenSwapURL = URL(string: tokenSwapLocal)
        config.tokenRefreshURL = URL(string: tokenRefreshLocal)
        //config.playURI = "spotify:track:79zshLLUoqncCBkd9YfVUE"
        return config
    }()
    
    lazy var sessionManager: SPTSessionManager = {
        let manager = SPTSessionManager(configuration: configuration, delegate: self)
        manager.alwaysShowAuthorizationDialog = false
        return manager
    }()
    
    private override init() {
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(self.authenticate), name: spotifyAuthenticate, object: nil)
    }
}


// MARK: - Spotify Support
extension SpotifySessionManager: SPTSessionManagerDelegate {
    
    @objc func authenticate() {
        //print("auth")
        let requestedScope: SPTScope = [.appRemoteControl, .playlistReadPrivate, .userTopRead]
        self.sessionManager.initiateSession(with: requestedScope, options: .default)
    }
    
    func sessionManager(manager: SPTSessionManager, didInitiate session: SPTSession) {
        print("Success", session)
        let sptConnParam = SPTAppRemoteConnectionParams(accessToken: session.accessToken, defaultImageSize: CGSize(width: 100, height: 100), imageFormat: .any)
        appRemote = SPTAppRemote(configuration: configuration, connectionParameters: sptConnParam, logLevel: .debug)
        DispatchQueue.main.async {
            self.appRemote?.delegate = self
            #if !targetEnvironment(simulator)
            self.appRemote?.connect()
            #else
            NotificationCenter.default.post(name: spotifyAuthenticated, object: nil)
            #endif
        }
    }
    
    func sessionManager(manager: SPTSessionManager, didFailWith error: Error) {
        print("fail", error)
    }
    
    func sessionManager(manager: SPTSessionManager, didRenew session: SPTSession) {
        print("renewed", session)
        if appRemote != nil {
            appRemote?.connectionParameters.accessToken = session.accessToken
            //appRemote?.connect()
        }
    }
}

extension SpotifySessionManager: SPTAppRemoteDelegate, SPTAppRemotePlayerStateDelegate {
    func playerStateDidChange(_ playerState: SPTAppRemotePlayerState) {
        NotificationCenter.default.post(name: NSNotification.Name("playerStateChange"), object: nil, userInfo: ["state": playerState])
        //print(playerState.contextTitle)
    }
    
    func appRemoteDidEstablishConnection(_ appRemote: SPTAppRemote) {
        appRemote.playerAPI?.delegate = self
        appRemote.playerAPI?.subscribe(toPlayerState: { (success, error) in
            if let error = error {
                print("Error subscribing to player state:" + error.localizedDescription)
            }
        })
        
        NotificationCenter.default.post(name: spotifyAuthenticated, object: nil)
        print("Connection established")
    }
    
    func appRemote(_ appRemote: SPTAppRemote, didFailConnectionAttemptWithError error: Error?) {
        print("AppRemote: ", error!.localizedDescription)
    }
    
    func appRemote(_ appRemote: SPTAppRemote, didDisconnectWithError error: Error?) {
        print(error!.localizedDescription)
    }
    
    
}
