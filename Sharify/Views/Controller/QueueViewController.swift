//
//  QueueViewController.swift
//  Sharify
//
//  Created by Maximilian Seiferth on 06.12.19.
//  Copyright © 2019 Maximilian Seiferth. All rights reserved.
//

import UIKit
import MediaPlayer

class QueueViewController: UIViewController {
    var tableView: UITableView?
    let playerView = PlayerView()

    let sharifyClient = SharifyClient()
    let spotifyClient = SpotifyClient()
    var queueInviteToken: String?
    var isHostingQueue = false

    var queueInfo: QueueInfoResult?
    var lastPlayerState: SPTAppRemotePlayerState?
    var trackTimer: Timer?
    var queueIsActive = false
    var currentItem: SPTAppRemoteTrack?
    var timerStart: Date?

    override func viewDidLoad() {
        super.viewDidLoad()

        //Init NotificationObserver
        NotificationCenter.default.addObserver(self, selector: #selector(self.addTrack(notification:)), name: Notification.Name("addTrack"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerStateDidChanged(notification:)), name: Notification.Name("playerStateChange"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.playBtnPressed), name: Notification.Name("playBtnPressed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.skipBtnPressed), name: Notification.Name("skipBtnPressed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.queueStartBtnPressed), name: Notification.Name("queueStartBtnPressed"), object: nil)

        //View Setup
        self.title = "My Queue"
        self.view.backgroundColor = UIColor(red:0.07, green:0.07, blue:0.07, alpha:1.0)

        //NavigationBar
        if #available(iOS 12, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
        }
        
        //self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.backgroundColor = .black
        self.navigationController?.navigationBar.barStyle = .blackOpaque
        self.navigationController?.navigationBar.isTranslucent = false
        let refreshBtn = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(fetchQuueInfo))
        self.navigationItem.rightBarButtonItem = refreshBtn
        
        //TableView
        self.tableView = UITableView(frame: self.view.frame)
        self.tableView?.backgroundColor = UIColor(red:0.07, green:0.07, blue:0.07, alpha:1.0)
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        self.tableView?.register(QueueTableViewCell.self, forCellReuseIdentifier: "Cell")
        self.tableView?.tableFooterView = UIView(frame: .zero)
        self.view.addSubview(tableView!)
        

        
        //PlayerView + QueueStart Header View
        if isHostingQueue {
            let queueStartTableHeader = QueueStartTableHeaderView()
            queueStartTableHeader.frame.size = CGSize(width: self.view.frame.width, height: 70)
            self.tableView?.tableHeaderView = queueStartTableHeader
            
            self.view.addSubview(playerView)
            playerView.translatesAutoresizingMaskIntoConstraints = false
            playerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
            playerView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
            playerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
            playerView.heightAnchor.constraint(equalToConstant: 83).isActive = true
        }
        
        let addButton = UIButton()
        addButton.layer.cornerRadius = 30
        addButton.backgroundColor = UIColor(red:0.19, green:0.19, blue:0.19, alpha:1.0)
        if #available(iOS 13.0, *) {
            addButton.setImage(UIImage(systemName: "plus", withConfiguration: UIImage.SymbolConfiguration(scale: .large)), for: .normal)
        } else {
            addButton.setTitle("+", for: .normal)
            addButton.titleLabel?.textColor = .white
            addButton.titleLabel?.font = UIFont.systemFont(ofSize: 25.0, weight: .regular)
        }
        
        addButton.tintColor = UIColor(red:0.35, green:0.78, blue:0.98, alpha:1.0)
        addButton.addTarget(self, action: #selector(openSearchController), for: .touchUpInside)
        self.view.addSubview(addButton)
         
        addButton.translatesAutoresizingMaskIntoConstraints = false
        addButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -10.0).isActive = true
        if isHostingQueue {
            addButton.bottomAnchor.constraint(equalTo: playerView.topAnchor, constant: -15.0).isActive = true
        } else {
            addButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -15.0).isActive = true
        }
        addButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        addButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        if #available(iOS 13, *) {
            let shareInviteButton = UIButton()
            shareInviteButton.layer.cornerRadius = 30
            shareInviteButton.backgroundColor = UIColor(red:0.19, green:0.19, blue:0.19, alpha:1.0)
            shareInviteButton.setImage(UIImage(systemName: "qrcode", withConfiguration: UIImage.SymbolConfiguration(scale: .large)), for: .normal)
            shareInviteButton.tintColor = UIColor(red:0.35, green:0.78, blue:0.98, alpha:1.0)
            shareInviteButton.addTarget(self, action: #selector(showShareQRCode), for: .touchUpInside)
            self.view.addSubview(shareInviteButton)
            
            shareInviteButton.translatesAutoresizingMaskIntoConstraints = false
            shareInviteButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -10.0).isActive = true
            shareInviteButton.bottomAnchor.constraint(equalTo: addButton.topAnchor, constant: -15.0).isActive = true
            shareInviteButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
            shareInviteButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
         }
        
        self.fetchQuueInfo()
        self.fetchPlayerState() { playerState in
            //Nothing ToDo
        }
        
        let _ = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.fetchQuueInfo), userInfo: nil, repeats: true)
    }
    
    @objc func showShareQRCode() {
        let shareViewController = ShareQueueViewController()
        shareViewController.inviteToken = self.queueInviteToken
        self.present(shareViewController, animated: true)
    }
    
    @objc func openSearchController() {
        let searchView = SearchViewController()
        let navController = UINavigationController(rootViewController: searchView)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true)
    }
    
    @objc func playerStateDidChanged(notification: NSNotification) {
        if isHostingQueue {
            if let state = notification.userInfo?["state"] as? SPTAppRemotePlayerState {
                updateView(playerState: state)
            }
        }
    }
    
    func updateView(playerState: SPTAppRemotePlayerState) {
        if lastPlayerState?.track.uri != playerState.track.uri {
            
            #if DEBUG
            let duration = Double(playerState.track.duration)
            print("Duration in ms: ", duration)
            
            let durationinSec = duration / 1000
            print("Duration in seconds: ", durationinSec)
            #endif
            
            if queueIsActive {
                timerStart = Date()
                trackTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.timerTick), userInfo: nil, repeats: true)
            }
            
            fetchArtwork(for: playerState.track)
            playerView.trackTitleLabel.text = playerState.track.name
            playerView.trackArtistLabel.text = playerState.track.artist.name
            currentItem = playerState.track
        }
        
        self.lastPlayerState = playerState
        if playerState.isPaused {
            if #available(iOS 13.0, *) {
                playerView.playBtn.setImage(UIImage(systemName: "play.fill", withConfiguration: UIImage.SymbolConfiguration(scale: .large)), for: .normal)
            } else {
                // Fallback on earlier versions
            }
        } else {
            if #available(iOS 13.0, *) {
                playerView.playBtn.setImage(UIImage(systemName: "pause.fill", withConfiguration: UIImage.SymbolConfiguration(scale: .large)), for: .normal)
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    @objc func timerTick() {
        let now = Date()
        
        let seconds = Int(now.timeIntervalSince(self.timerStart!))
        let trackDuration = Int(TimeInterval((Int(currentItem!.duration) / 1000)))
        let remaining = trackDuration - seconds
        
        self.playerView.setProgress(duration: trackDuration, remaining: seconds)
        
        #if DEBUG
            print("Current: ", seconds)
            print("Remaining: ", remaining)
        #endif
        
        if remaining < 2 {
            print("Time elapsed, next track!")
            trackTimer?.invalidate()
            self.playNextTrack()
        }
    }
    
    func checkSpotifyApp() -> Bool {
        if SpotifySessionManager.shared.appRemote!.isConnected {
            return true
        } else {
            SpotifySessionManager.shared.authenticate()
            return true
        }
    }
    
    func playNextTrack() {
        queueIsActive = true
        
        sharifyClient.advanceQueue() { result in
            switch result {
            case .success(let result):
                print(result!.nextSongID)
                let id = "spotify:track:" + result!.nextSongID
                SpotifySessionManager.shared.appRemote?.playerAPI?.play(id, callback: { (callresult, error) in
                    guard error == nil else {
                        print(error!.localizedDescription)
                        return
                    }
                })
                self.fetchQuueInfo()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    @objc func queueStartBtnPressed() {
        if !SpotifySessionManager.shared.appRemote!.isConnected {
            SpotifySessionManager.shared.authenticate()
            #if !targetEnvironment(simulator)
            SpotifySessionManager.shared.appRemote?.connect()
            #endif
        }
        
        if queueIsActive == false {
            playNextTrack()
            self.tableView?.tableHeaderView = nil
            enablePlayerControls()
            return
        }
    }
    
    @objc func skipBtnPressed() {
        if queueIsActive {
            playNextTrack()
        }
    }
    
    @objc func playBtnPressed() {
        if !SpotifySessionManager.shared.appRemote!.isConnected {
            SpotifySessionManager.shared.authenticate()
            //SpotifySessionManager.shared.appRemote?.connect()
        }
        
        fetchPlayerState() { playerState in
            if playerState!.isPaused {
                SpotifySessionManager.shared.appRemote?.playerAPI?.resume()
            } else {
                SpotifySessionManager.shared.appRemote?.playerAPI?.pause()
            }
        }
    }
    
    func enablePlayerControls() {
        NotificationCenter.default.post(name: NSNotification.Name("enablePlayerControls"), object: nil, userInfo: nil)
    }
    
    @objc func addTrack(notification: Notification) {
        if let track = notification.userInfo?["track"] as? TrackItem {
            let queueCount = self.queueInfo?.queue.elements.count
            
            sharifyClient.addTrack(track: track) { result in
                switch result {
                case .success(let result):
                    self.queueInfo = result
                    if (result?.queue.elements.count)! > 0 {
                        self.fetchTrackInfo(tracks: (result?.queue.elements)!) { result in
                           switch result {
                           case .success(let result):
                               self.queueInfo?.queue.queueTracks = result?.tracks
                               DispatchQueue.main.async {
                                    if queueCount != nil && ((queueCount ?? -1) + 1) >= 3 {
                                        NotificationCenter.default.post(name: NSNotification.Name("enableQueueStartBtn"), object: nil, userInfo: nil)
                                    }
                                    /*if (queueCount == 0 || self.queueInfo == nil) && self.queueIsActive == false {
                                        self.playNextTrack()
                                        self.queueIsActive = true
                                    }*/
                                   self.tableView?.reloadData()
                               }
                           case .failure(let error):
                               print(error.localizedDescription)
                           }
                        }
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    @objc func fetchQuueInfo() {
        self.queueInfo = nil
        
        sharifyClient.getQueueInfo() { result in
            switch result {
            case .success(let result):
                self.queueInfo = result
                if (result?.queue.elements.count)! > 0 {
                    self.fetchTrackInfo(tracks: (result?.queue.elements)!) { result in
                       switch result {
                       case .success(let result):
                           self.queueInfo?.queue.queueTracks = result?.tracks
                           DispatchQueue.main.async {
                                if (result?.tracks.count)! >= 3 {
                                    NotificationCenter.default.post(name: NSNotification.Name("enableQueueStartBtn"), object: nil, userInfo: nil)
                                }
                                self.tableView?.reloadData()
                           }
                       case .failure(let error):
                           print(error.localizedDescription)
                       }
                    }
                }
                
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func fetchPlayerState(completion: @escaping (SPTAppRemotePlayerState?) -> Void) {
        SpotifySessionManager.shared.appRemote?.playerAPI?.getPlayerState({ [weak self] (playerState, error) in
            if let error = error {
                print("Error getting player state:" + error.localizedDescription)
            } else if let playerState = playerState as? SPTAppRemotePlayerState {
                self?.updateView(playerState: playerState)
                completion(playerState)
            }
        })
        
    }
    
    func fetchTrackInfo(tracks: [QueueElement], completion: @escaping (Result<SeveralTracksResult?, APIError>) -> Void) {
        var trackIDs: [String] = []
        for element in tracks {
            trackIDs.append(element.spotifyID)
        }
        
        spotifyClient.getTracks(tracks: trackIDs, completion: completion)
    }

    
    func fetchArtwork(for track:SPTAppRemoteTrack) {
        SpotifySessionManager.shared.appRemote?.imageAPI?.fetchImage(forItem: track, with: CGSize.zero, callback: { (image, error) in
            if let error = error {
                print("Error fetching track image: " + error.localizedDescription)
            } else if let image = image as? UIImage {
                self.playerView.coverView.image = image
            }
        })
    }
    
}

extension QueueViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = .black
        let headerview = view as! UITableViewHeaderFooterView
        headerview.textLabel?.textColor = .gray
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Queued tracks"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.queueInfo?.queue.queueTracks?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! QueueTableViewCell
        let track = queueInfo?.queue.queueTracks![indexPath.row]
        cell.trackName.text = track?.name
        cell.artistLabel.text = track?.artists[0].name
        cell.coverPreview.sd_setImage(with: URL(string: (track?.album.images[2].url)!))
        if #available(iOS 13.0, *) {
            //cell.accessoryView = UIImageView(image: UIImage(systemName: "line.horizontal.3", withConfiguration: UIImage.SymbolConfiguration(scale: .large)))
        } else {
            // Fallback on earlier versions
        }
        
        return cell
    }
}
