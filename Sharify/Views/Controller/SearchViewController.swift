//
//  SearchViewController.swift
//  Sharify
//
//  Created by Maximilian Seiferth on 25.11.19.
//  Copyright © 2019 Maximilian Seiferth. All rights reserved.
//

import UIKit
import SDWebImage


class SearchViewController: UIViewController {

    //Shared classes
    let spotifySessionManager = SpotifySessionManager.shared
    let spotifyAPIClient = SpotifyClient()
    
    //View
    var tableView: UITableView?
    var searchController: UISearchController?
    
    //Data
    var searchResults: SearchResult?
    var userTopResult: UserTopTracksResult?
    var items: [TrackItem]?
    
    //Support
    var isSearchBarEmpty: Bool {
        return searchController!.searchBar.text?.isEmpty ?? true
    }
    
    var isFiltering: Bool {
      return searchController!.isActive && !isSearchBarEmpty
    }

    let threshold: CGFloat = 100.0 // threshold from bottom of tableView
    var isLoadingMore = false // flag
    var currentOffset = 30
    let maxOffset = 30
    var searchQuery = ""
    var keyCount = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Search"
        
        if #available(iOS 12, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
        }
        
        //self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.backgroundColor = .black
        self.navigationController?.navigationBar.barStyle = .blackOpaque
        self.navigationController?.navigationBar.isTranslucent = false
        
        let dismissBtn = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissView))

        self.navigationItem.leftBarButtonItem = dismissBtn
        
        self.view.backgroundColor = UIColor(red:0.07, green:0.07, blue:0.07, alpha:1.0)

        searchController = UISearchController(searchResultsController: nil)
        searchController?.searchResultsUpdater = self
        searchController?.searchBar.delegate = self
        searchController?.obscuresBackgroundDuringPresentation = false
        searchController?.searchBar.placeholder = "Search"
        navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationItem.searchController = self.searchController!
        definesPresentationContext = true
        
        tableView = UITableView(frame: self.view.frame)
        tableView?.backgroundColor = UIColor(red:0.07, green:0.07, blue:0.07, alpha:1.0)
        tableView?.delegate = self
        tableView?.dataSource = self
        tableView?.register(SearchTableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView?.tableFooterView = UIView(frame: .zero)

        self.view.addSubview(self.tableView!)
        
        fetchTopTracks()
        
    }
    
    @objc func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func addTrack(sender: UIButton) {
        let clickedRow = sender.tag
        let dir = ["track" :  self.items![clickedRow]]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addTrack"), object: nil, userInfo: dir)
        dismissView()
    }
    
    func fetchTopTracks() {
        spotifyAPIClient.getUserTop(offset: String(describing: currentOffset)) { result in
            switch result {
            case .success(let userTopResult):
                guard let userTopResult = userTopResult else { return }
                self.userTopResult = userTopResult
                
                if self.items != nil {
                    self.items?.append(contentsOf: userTopResult.items)
                } else {
                    self.items = userTopResult.items
                }
                
                self.tableView?.reloadData()
                self.tableView?.layoutSubviews()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func doSearch(query: String) {
        self.searchQuery = query
        if query == "" {
            return
        }
        spotifyAPIClient.search(query: query, offset: String(describing: currentOffset)) { result in
            switch result {
            case .success(let searchResult):
                guard let searchResult = searchResult else { return }
                self.searchResults = searchResult
                
                if self.items != nil {
                    self.items?.append(contentsOf: searchResult.tracks!.items)
                } else {
                    self.items = searchResult.tracks?.items
                }
                self.tableView?.reloadData()
                self.tableView?.layoutSubviews()
                self.isLoadingMore = false
            case .failure(let error):
                print("Error: ", error.localizedDescription)
            }
        }
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isFiltering {
            return "Search results"
        } else {
            return "Your Spotify top tracks"
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = .black
        let headerview = view as! UITableViewHeaderFooterView
        headerview.textLabel?.textColor = .gray
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        /*if isFiltering {
            return self.searchResults?.tracks?.items.count ?? 0
        } else {
            return userTopResult?.items.count ?? 0
        }*/
        
        return self.items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SearchTableViewCell
        /*var searchItem = self.userTopResult?.items[indexPath.row]
        if isFiltering {
            searchItem = self.searchResults?.tracks?.items[indexPath.row]
        }*/
        let searchItem = self.items?[indexPath.row]
        if searchItem != nil {
            cell.trackName.text = searchItem?.name
            cell.artistLabel.text = searchItem?.artists[0].name
            cell.coverPreview.sd_setImage(with: URL(string: (searchItem?.album.images[2].url)!))
            cell.addButton.addTarget(self, action: #selector(addTrack(sender:)), for: .touchUpInside)
            cell.addButton.tag = indexPath.row
        }

        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
             
        if !isLoadingMore && (maximumOffset - contentOffset <= threshold) && (currentOffset != maxOffset) {
            // Get more data - API call
            self.isLoadingMore = true
            self.currentOffset += 10
            if isFiltering {
                doSearch(query: searchQuery)
            } else {
                fetchTopTracks()
            }
            
        }
    }
    
}

extension SearchViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        self.keyCount += 1
        if self.keyCount != 0 && (self.keyCount % 2 == 0) {
            self.items = nil
            currentOffset = 0
            doSearch(query: searchController.searchBar.text ?? "")
        }
    }
}

extension SearchViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.keyCount = 0
        currentOffset = 0
        fetchTopTracks()
    }
}
