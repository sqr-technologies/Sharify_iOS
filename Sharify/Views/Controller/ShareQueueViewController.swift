//
//  ShareQueueViewController.swift
//  QShare
//
//  Created by Maximilian Seiferth on 24.12.19.
//  Copyright © 2019 Maximilian Seiferth. All rights reserved.
//

import UIKit

class ShareQueueViewController: UIViewController {
    var gradientOverlayView: UIView?
    var textLabel = UILabel()
    var dismissBtn = UIButton()
    var qrImageView = UIImageView()
    var inviteToken: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor(red:0.33, green:0.20, blue:0.01, alpha:1.0).cgColor, UIColor(red:0.66, green:0.00, blue:0.49, alpha:1.0).cgColor]
        gradient.locations = [0.0, 1.0]
        
        gradientOverlayView = UIView()
        gradientOverlayView?.frame = self.view.frame
        self.view.addSubview(gradientOverlayView!)
        
        gradient.frame = (gradientOverlayView?.frame)!
        gradientOverlayView?.layer.insertSublayer(gradient, at: 0)
        
        if #available(iOS 13.0, *) {
            dismissBtn.setImage(UIImage.init(systemName: "xmark", withConfiguration: UIImage.SymbolConfiguration(scale: .large)), for: .normal)
        }
        dismissBtn.tintColor = .white
        dismissBtn.addTarget(self, action: #selector(dismissView), for: .touchUpInside)
        self.gradientOverlayView?.addSubview(dismissBtn)
        
        
        dismissBtn.translatesAutoresizingMaskIntoConstraints = false
        dismissBtn.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20.0).isActive = true
        dismissBtn.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 20.0).isActive = true
        
        qrImageView.isHidden = true
        
        textLabel.text = "Let your friends join the queue by scanning this QR Code"
        textLabel.textColor = .white
        textLabel.font = UIFont.systemFont(ofSize: 30.0, weight: .light)
        textLabel.textAlignment = .center
        textLabel.numberOfLines = 3
        textLabel.sizeToFit()
        
        self.view.addSubview(qrImageView)
        self.view.addSubview(textLabel)
        
        qrImageView.translatesAutoresizingMaskIntoConstraints = false
        qrImageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        qrImageView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -100.0).isActive = true
        qrImageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        qrImageView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20.0).isActive = true
        textLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20.0).isActive = true
        textLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        textLabel.centerYAnchor.constraint(equalTo: qrImageView.bottomAnchor, constant: 70.0).isActive = true
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        generateQRCode()
    }
    
    @objc func dismissView() {
        self.dismiss(animated: true)
    }

    func generateQRCode() {
        if inviteToken != "" {
            // Get define string to encode
            // Get data from the string
            let data = self.inviteToken?.data(using: String.Encoding.ascii)
            // Get a QR CIFilter
            guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return }
            // Input the data
            qrFilter.setValue(data, forKey: "inputMessage")
            // Get the output image
            guard let qrImage = qrFilter.outputImage else { return }
            
            
            // Scale the image
            let transform = CGAffineTransform(scaleX: 10, y: 10)
            let scaledQrImage = qrImage.transformed(by: transform)
            // Invert the colors
            guard let colorInvertFilter = CIFilter(name: "CIColorInvert") else { return }
            colorInvertFilter.setValue(scaledQrImage, forKey: "inputImage")
            guard let outputInvertedImage = colorInvertFilter.outputImage else { return }
            // Replace the black with transparency
            guard let maskToAlphaFilter = CIFilter(name: "CIMaskToAlpha") else { return }
            maskToAlphaFilter.setValue(outputInvertedImage, forKey: "inputImage")
            guard let outputCIImage = maskToAlphaFilter.outputImage else { return }
            // Do some processing to get the UIImage
            let context = CIContext()
            guard let cgImage = context.createCGImage(outputCIImage, from: outputCIImage.extent) else { return }
            
            
            let processedImage = UIImage(cgImage: cgImage)
            self.qrImageView.image = processedImage
            self.qrImageView.isHidden = false
        }
    }
}
