//
//  StartQueueSelectViewController.swift
//  Sharify
//
//  Created by Maximilian Seiferth on 06.12.19.
//  Copyright © 2019 Maximilian Seiferth. All rights reserved.
//

import UIKit

class StartQueueSelectViewController: UIViewController {
    var tableView: UITableView?
    let sharifyClient = SharifyClient()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.foundQRToken(notification:)), name: Notification.Name("qrTokenFound"), object: nil)

        self.title = "Queue"
        
        if #available(iOS 12, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
        }
        
        //self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.backgroundColor = .black
        self.navigationController?.navigationBar.barStyle = .blackOpaque
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.view.backgroundColor = UIColor(red:0.07, green:0.07, blue:0.07, alpha:1.0)
        
        
        self.tableView = UITableView(frame: self.view.frame)
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        self.tableView?.backgroundColor = UIColor(red:0.07, green:0.07, blue:0.07, alpha:1.0)
        self.tableView?.tableFooterView = UIView(frame: .zero)
        self.view.addSubview(self.tableView!)
    }

    
    @objc func foundQRToken(notification: Notification) {
        if let token = notification.userInfo?["token"] {
            joinQueue(token: token as! String)
        }
    }

}

extension StartQueueSelectViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
           view.tintColor = .black
           let headerview = view as! UITableViewHeaderFooterView
           headerview.textLabel?.textColor = .gray
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Create a queue"
        case 1:
            return "Join a queue"
        default:
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")

        switch indexPath.section {
        case 0:
            cell.textLabel?.text = "Create queue"
            cell.textLabel?.textColor = .white
            cell.backgroundColor = UIColor(red:0.11, green:0.11, blue:0.11, alpha:1.0)
            cell.accessoryType = .disclosureIndicator
        case 1:
            //let cell = JoinQueueTableViewCell(style: .default, reuseIdentifier: "cell")
            //cell.textLabel?.text = "Join queue"
            
            cell.textLabel?.text = "Join queue"
            cell.textLabel?.textColor = .white
            cell.backgroundColor = UIColor(red:0.11, green:0.11, blue:0.11, alpha:1.0)
            cell.accessoryType = .disclosureIndicator
        default:
            return UITableViewCell()
        }
        
        return cell
        //return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            createQueue()
        case 1:
            openQRCamera()
        default:
            break
        }
    }
    
    
    func createQueue() {
        var directory = Dictionary<String, String>()
        directory["displayName"] = "Test"
        sharifyClient.createNewQueue(queueParams: directory) { result in
            switch result {
            case .success(let result):
                DispatchQueue.main.async {
                    let queueViewController = QueueViewController()
                    queueViewController.isHostingQueue = true
                    queueViewController.queueInviteToken = result?.invite_token
                    let navController = UINavigationController(rootViewController: queueViewController)
                    navController.modalPresentationStyle = .fullScreen
                    self.present(navController, animated: true)
                }
                //print(result?.invite_token)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func joinQueue(token: String) {
        sharifyClient.joinQueue(token: token) { result in
            switch result {
            case .success(_):
                DispatchQueue.main.async {
                    let queueViewController = QueueViewController()
                    queueViewController.isHostingQueue = false
                    queueViewController.queueInviteToken = token
                    let navController = UINavigationController(rootViewController: queueViewController)
                    navController.modalPresentationStyle = .fullScreen
                    self.present(navController, animated: true)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func openQRCamera() {
        let QRScanner = QRScannerViewController()
        self.present(QRScanner, animated: true, completion: nil)
    }
}

class JoinQueueTableViewCell: UITableViewCell {
    var queueIDTxt = UITextField()
    var joinQueueButton = UIButton()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor(red:0.11, green:0.11, blue:0.11, alpha:1.0)
        
        self.addSubview(queueIDTxt)
        self.addSubview(joinQueueButton)
        
        self.queueIDTxt.borderStyle = .none
        self.queueIDTxt.placeholder = "Type queue id here"
        self.queueIDTxt.textColor = .white
        
        self.queueIDTxt.translatesAutoresizingMaskIntoConstraints = false
        self.queueIDTxt.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.0).isActive = true
        self.queueIDTxt.topAnchor.constraint(equalTo: self.topAnchor, constant: 2.0).isActive = true
        self.queueIDTxt.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 2.0).isActive = true
        self.queueIDTxt.widthAnchor.constraint(equalToConstant: 300).isActive = true
        //self.queueIDTxt.trailingAnchor.constraint(equalTo: self.joinQueueButton.leadingAnchor, constant: 5.0).isActive = true
        
        self.joinQueueButton.titleLabel?.text = "Join"
        self.joinQueueButton.translatesAutoresizingMaskIntoConstraints = false
        self.joinQueueButton.leadingAnchor.constraint(equalTo: self.queueIDTxt.trailingAnchor, constant: 5.0).isActive = true
        self.joinQueueButton.topAnchor.constraint(equalTo: self.topAnchor, constant: 2.0).isActive = true
        self.joinQueueButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 2.0).isActive = true
        self.joinQueueButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 10.0).isActive = true
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
