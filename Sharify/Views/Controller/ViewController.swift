//
//  ViewController.swift
//  Sharify
//
//  Created by Maximilian Seiferth on 23.12.18.
//  Copyright © 2018 Maximilian Seiferth. All rights reserved.
//

import UIKit
import SafariServices
import AuthenticationServices

class SpotifyButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            imageView?.frame.size = CGSize(width: 35, height: 35)
            imageEdgeInsets = UIEdgeInsets(top: 8, left: 15, bottom: 5, right: (bounds.width - 55))
            titleEdgeInsets = UIEdgeInsets(top: 5, left: (imageView?.frame.width)! - 35, bottom: 5, right: 5)
        } else {
            self.titleEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        }
        
        self.backgroundColor = UIColor(red: 0.11, green: 0.84, blue: 0.38, alpha: 1.0)
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.masksToBounds = true
        self.setTitleColor(.white, for: .normal)
        //self.titleEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
}

class ViewController: UIViewController {
    let client = SharifyClient()
    var session: ASWebAuthenticationSession?
    var gradientOverlayView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.authenticated), name: spotifyAuthenticated, object: nil)

        let gradient = CAGradientLayer()
        gradient.colors = [UIColor(red:0.04, green:0.00, blue:0.05, alpha:1.0).cgColor, UIColor(red:0.03, green:0.11, blue:0.18, alpha:1.0).cgColor]
        gradient.locations = [0.0, 1.0]
        
        gradientOverlayView = UIView()
        gradientOverlayView?.frame = self.view.frame
        self.view.addSubview(gradientOverlayView!)
//        gradientOverlayView?.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
//        gradientOverlayView?.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor).isActive = true
//        gradientOverlayView?.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
//        gradientOverlayView?.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        gradient.frame = (gradientOverlayView?.frame)!
        gradientOverlayView?.layer.insertSublayer(gradient, at: 0)
        
        let logo = UIImageView(image: UIImage(named:"Logo"))
        //logo.frame.size = CGSize(width: 1000, height: 1000)
        self.gradientOverlayView?.addSubview(logo)
        logo.translatesAutoresizingMaskIntoConstraints = false
        logo.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 150).isActive = true
        logo.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true

        let sloganLabel = UILabel()
        sloganLabel.text = "Partying 🥳 together? \nUse a Queue together!"
        sloganLabel.textAlignment = .center
        sloganLabel.textColor = .white
        sloganLabel.font = UIFont.systemFont(ofSize: 33, weight: .thin)
        sloganLabel.numberOfLines = 2
        self.gradientOverlayView?.addSubview(sloganLabel)
        sloganLabel.translatesAutoresizingMaskIntoConstraints = false
        sloganLabel.topAnchor.constraint(equalTo: logo.bottomAnchor, constant: 50).isActive = true
        sloganLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        let spotifyButton = SpotifyButton()
        spotifyButton.setImage(UIImage(named: "SpotifyLogoWhite"), for: .normal)
        spotifyButton.setTitle("Connect with Spotify", for: .normal)
        spotifyButton.titleLabel?.textColor = .white
        spotifyButton.addTarget(self, action: #selector(self.authenticate), for: .touchUpInside)
        self.gradientOverlayView?.addSubview(spotifyButton)
        spotifyButton.translatesAutoresizingMaskIntoConstraints = false
        spotifyButton.topAnchor.constraint(equalTo: sloganLabel.bottomAnchor, constant: 75).isActive = true
        spotifyButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        spotifyButton.widthAnchor.constraint(equalToConstant: 249).isActive = true
        spotifyButton.heightAnchor.constraint(equalToConstant: 52).isActive = true
        
        if #available(iOS 13, *) {
            self.view.backgroundColor = UIColor.systemBackground
            overrideUserInterfaceStyle = .dark
        }

        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @objc func authenticate() {
        NotificationCenter.default.post(name: spotifyAuthenticate, object: nil)
    }
    
    @objc func authenticated() {
        DispatchQueue.main.async {
            let searchViewController = StartQueueSelectViewController()
            let navController = UINavigationController(rootViewController: searchViewController)
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated: true)
        }
    }
    
    /*@objc func startAuthentication() {
        client.authorize_init() { result in
            switch result {
            case .success(let authorizeResult):
                guard let authResult = authorizeResult else { return }                
                
                self.session = ASWebAuthenticationSession.init(url: self.buildSpotifyAuthURL(result: authResult), callbackURLScheme: "sharify://auth_callback", completionHandler: { (callback: URL?, error: Error?) in
                    guard error == nil, let _ = callback else {
                        return
                    }
                    
                    self.client.fetchAuthTokens(state: authResult.state, completion: { result in
                        switch result {
                        case .success(let tokenResult):
                            
                            let defaults = UserDefaults.standard
                            defaults.set(tokenResult?.access_token, forKey: "access_token")
                            defaults.set(tokenResult?.refresh_token, forKey: "refresh_token")
                            
                        case .failure(let error):
                            print("Error: \(error)")
                        }
                    })
                    
                })
                self.session?.start()
                
            case .failure(let error):
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func buildSpotifyAuthURL(result: AuthorizeResult) -> URL! {
        var spotifyBaseURI = "https://accounts.spotify.com/authorize"
        
        spotifyBaseURI += "?state=\(result.state)"
        spotifyBaseURI += "&client_id=\(result.client_id)"
        spotifyBaseURI += "&scope=\(result.scope)"
        spotifyBaseURI += "&response_type=\(result.response_type)"
        spotifyBaseURI += "&redirect_uri=\(result.redirect_uri)"
        
        //print(spotifyBaseURI)
        return URL(string: spotifyBaseURI)
    }*/
}


/*
extension ViewController: SFSafariViewControllerDelegate {
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        guard let authorizeResult = aResult else { return }
        client.fetchAuthTokens(state: authorizeResult.state, completion: { result in
            switch result {
            case .success(let tokenResult):
                print(tokenResult)
            case .failure(let error):
                print("Error: \(error)")
            }
        })
    }
}*/
