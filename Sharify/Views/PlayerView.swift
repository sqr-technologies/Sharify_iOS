//
//  PlayerView.swift
//  QShare
//
//  Created by Maximilian Seiferth on 07.12.19.
//  Copyright © 2019 Maximilian Seiferth. All rights reserved.
//

import UIKit

class PlayerView: UIView {
    var coverView = UIImageView()
    var trackTitleLabel = UILabel()
    var trackArtistLabel = UILabel()
    var playBtn = UIButton()
    var skipBtn = UIButton()
    var progressBar = UIProgressView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.enablePlayerControls), name: Notification.Name("enablePlayerControls"), object: nil)
        
        self.backgroundColor = .black
        
        if #available(iOS 13.0, *) {
            playBtn.setImage(UIImage(systemName: "play.fill", withConfiguration: UIImage.SymbolConfiguration(scale: .large)), for: .normal)
            skipBtn.setImage(UIImage(systemName: "forward.end.fill", withConfiguration: UIImage.SymbolConfiguration(scale: .large)), for: .normal)
        } else {
            // Fallback on earlier versions
        }
        
        playBtn.tintColor = UIColor(red:0.35, green:0.78, blue:0.98, alpha:1.0)
        playBtn.addTarget(self, action: #selector(playBtnPressed(sender:)), for: .touchUpInside)
        playBtn.isEnabled = false
        
        skipBtn.tintColor = UIColor(red:0.35, green:0.78, blue:0.98, alpha:1.0)
        skipBtn.addTarget(self, action: #selector(skipBtnPressed(sender:)), for: .touchUpInside)
        skipBtn.isEnabled = false

        trackTitleLabel.text = "----"
        trackTitleLabel.textColor = .white
        trackArtistLabel.text = "----"
        trackArtistLabel.textColor = .gray
        coverView.image = UIImage(named: "Placeholder")
        progressBar.setProgress(0.5, animated: true)
        progressBar.progressTintColor = UIColor(red:0.35, green:0.78, blue:0.98, alpha:1.0)
        
            
        self.addSubview(progressBar)
        self.addSubview(coverView)
        self.addSubview(trackTitleLabel)
        self.addSubview(trackArtistLabel)
        self.addSubview(playBtn)
        self.addSubview(skipBtn)
        
        progressBar.translatesAutoresizingMaskIntoConstraints = false
        progressBar.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        progressBar.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        progressBar.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        
        coverView.translatesAutoresizingMaskIntoConstraints = false
        coverView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 15.0).isActive = true
        coverView.topAnchor.constraint(equalTo: progressBar.topAnchor, constant: 10.0).isActive = true
        coverView.widthAnchor.constraint(equalToConstant: 40).isActive = true
        coverView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        skipBtn.translatesAutoresizingMaskIntoConstraints = false
        skipBtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.0).isActive = true
        skipBtn.centerYAnchor.constraint(equalTo: coverView.centerYAnchor).isActive = true
        
        playBtn.translatesAutoresizingMaskIntoConstraints = false
        playBtn.trailingAnchor.constraint(equalTo: skipBtn.leadingAnchor, constant: -20.0).isActive = true
        playBtn.centerYAnchor.constraint(equalTo: coverView.centerYAnchor).isActive = true
        
        trackTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        trackTitleLabel.leadingAnchor.constraint(equalTo: coverView.trailingAnchor, constant: 15.0).isActive = true
        trackTitleLabel.topAnchor.constraint(equalTo: coverView.topAnchor).isActive = true
        //trackTitleLabel.trailingAnchor.constraint(equalTo: playBtn.leadingAnchor, constant: -10.0).isActive = true
        //trackTitleLabel.trailingAnchor.constraint(equalTo: playBtn.leadingAnchor, constant: 5.0).isActive = true
        //trackTitleLabel.widthAnchor.constraint(equalToConstant: <#T##CGFloat#>)
        
        trackArtistLabel.translatesAutoresizingMaskIntoConstraints = false
        trackArtistLabel.leadingAnchor.constraint(equalTo: coverView.trailingAnchor, constant: 15.0).isActive = true
        //trackArtistLabel.topAnchor.constraint(equalTo: trackTitleLabel.bottomAnchor, constant: 5.0).isActive = true
        trackArtistLabel.bottomAnchor.constraint(equalTo: coverView.bottomAnchor).isActive = true
    }
    
    func setProgress(duration: Int, remaining: Int) {
        let progress: Float = Float(remaining / duration)
        self.progressBar.setProgress(progress, animated: true)
    }
    
    @objc func playBtnPressed(sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name("playBtnPressed"), object: nil, userInfo: nil)
    }
    
    @objc func skipBtnPressed(sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name("skipBtnPressed"), object: nil, userInfo: nil)
    }
    
    @objc func enablePlayerControls() {
        playBtn.isEnabled = true
        skipBtn.isEnabled = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
