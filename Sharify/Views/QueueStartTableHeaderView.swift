//
//  QueueStartTableHeaderView.swift
//  QShare
//
//  Created by Maximilian Seiferth on 23.12.19.
//  Copyright © 2019 Maximilian Seiferth. All rights reserved.
//

import UIKit

class QueueStartTableHeaderView: UIView {

    var label = UILabel()
    var startBtn = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.enableStartBtn), name: Notification.Name("enableQueueStartBtn"), object: nil)

        
        self.backgroundColor = UIColor(red:0.07, green:0.07, blue:0.07, alpha:1.0)
        
        startBtn.backgroundColor = UIColor(red:0.35, green:0.78, blue:0.98, alpha:1.0)
        startBtn.titleLabel?.textColor = .white
        startBtn.setTitle("Start Queue", for: .normal)
        startBtn.isEnabled = false
        
        startBtn.contentEdgeInsets = UIEdgeInsets(top: 5.0, left: 20.0, bottom: 5.0, right: 20.0)
        startBtn.layer.cornerRadius = 5.0
        startBtn.layer.masksToBounds = false
        startBtn.sizeToFit()
        startBtn.addTarget(self, action: #selector(startBtnPressed), for: .touchUpInside)
        
        
        label.text = "Add at least three Songs to your Queue"
        label.textColor = .white
        label.numberOfLines = 2
        label.sizeToFit()
        
        self.addSubview(label)
        self.addSubview(startBtn)
        
        startBtn.translatesAutoresizingMaskIntoConstraints = false
        startBtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.0).isActive = true
        startBtn.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        startBtn.widthAnchor.constraint(equalToConstant: startBtn.frame.width).isActive = true
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.0).isActive = true
        label.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        label.trailingAnchor.constraint(equalTo: startBtn.leadingAnchor, constant: -10.0).isActive = true

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func enableStartBtn() {
        startBtn.isEnabled = true
        startBtn.backgroundColor = .blue
    }
    
    @objc func startBtnPressed() {
        NotificationCenter.default.post(name: NSNotification.Name("queueStartBtnPressed"), object: nil, userInfo: nil)
    }
}
