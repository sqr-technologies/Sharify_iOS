//
//  QueueTableViewCell.swift
//  QShare
//
//  Created by Maximilian Seiferth on 07.12.19.
//  Copyright © 2019 Maximilian Seiferth. All rights reserved.
//

import UIKit

class QueueTableViewCell: UITableViewCell {
    var coverPreview = UIImageView()
    var trackName = UILabel()
    var artistLabel = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.backgroundColor = UIColor(red:0.11, green:0.11, blue:0.11, alpha:1.0)
        self.trackName.textColor = UIColor.white
        self.artistLabel.textColor = UIColor.gray

        self.artistLabel.font = UIFont.systemFont(ofSize: 15.0)

        self.addSubview(coverPreview)
        self.addSubview(trackName)
        self.addSubview(artistLabel)

        self.coverPreview.translatesAutoresizingMaskIntoConstraints = false
        self.coverPreview.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.coverPreview.widthAnchor.constraint(equalToConstant: 50).isActive = true
        self.coverPreview.topAnchor.constraint(equalTo: self.topAnchor, constant: 10.0).isActive = true
        self.coverPreview.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10.0).isActive = true
        //self.coverPreview.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 5.0).isActive = true

        self.trackName.translatesAutoresizingMaskIntoConstraints = false
        self.trackName.topAnchor.constraint(equalTo: self.topAnchor, constant: 10.0).isActive = true
        self.trackName.leadingAnchor.constraint(equalTo: self.coverPreview.trailingAnchor, constant: 10.0).isActive = true
        self.trackName.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -60.0).isActive = true

        self.artistLabel.translatesAutoresizingMaskIntoConstraints = false
        self.artistLabel.topAnchor.constraint(equalTo: self.trackName.bottomAnchor, constant: 5.0).isActive = true
        self.artistLabel.leadingAnchor.constraint(equalTo: self.coverPreview.trailingAnchor, constant: 10.0).isActive = true
        self.artistLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 10.0).isActive = true
        self.artistLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -15.0).isActive = true

    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
