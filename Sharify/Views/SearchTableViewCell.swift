//
//  SearchTableViewCell.swift
//  Sharify
//
//  Created by Maximilian Seiferth on 28.11.19.
//  Copyright © 2019 Maximilian Seiferth. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {
    var coverPreview = UIImageView()
    var trackName = UILabel()
    var artistLabel = UILabel()
    var addButton = UIButton()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor(red:0.11, green:0.11, blue:0.11, alpha:1.0)
        self.trackName.textColor = UIColor.white
        self.artistLabel.textColor = UIColor.gray
        
        self.artistLabel.font = UIFont.systemFont(ofSize: 15.0)
        
        if #available(iOS 13, *) {
            self.addButton.setImage(UIImage(systemName: "plus.circle", withConfiguration: UIImage.SymbolConfiguration(scale: .large)), for: .normal)
        } else {
            self.addButton.setTitle("+", for: .normal)
            self.addButton.titleLabel?.textColor = .white
            self.addButton.titleLabel?.font = UIFont.systemFont(ofSize: 25.0, weight: .regular)
        }
        
        self.addSubview(coverPreview)
        self.addSubview(trackName)
        self.addSubview(artistLabel)
        self.addSubview(addButton)
        
        self.coverPreview.translatesAutoresizingMaskIntoConstraints = false
        self.coverPreview.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.coverPreview.widthAnchor.constraint(equalToConstant: 50).isActive = true
        self.coverPreview.topAnchor.constraint(equalTo: self.topAnchor, constant: 10.0).isActive = true
        self.coverPreview.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10.0).isActive = true
        //self.coverPreview.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 5.0).isActive = true
        
        self.trackName.translatesAutoresizingMaskIntoConstraints = false
        self.trackName.topAnchor.constraint(equalTo: self.topAnchor, constant: 10.0).isActive = true
        self.trackName.leadingAnchor.constraint(equalTo: self.coverPreview.trailingAnchor, constant: 10.0).isActive = true
        self.trackName.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -60.0).isActive = true
        
        self.artistLabel.translatesAutoresizingMaskIntoConstraints = false
        self.artistLabel.topAnchor.constraint(equalTo: self.trackName.bottomAnchor, constant: 5.0).isActive = true
        self.artistLabel.leadingAnchor.constraint(equalTo: self.coverPreview.trailingAnchor, constant: 10.0).isActive = true
        self.artistLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 10.0).isActive = true
        self.artistLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -15.0).isActive = true
        
        self.addButton.translatesAutoresizingMaskIntoConstraints = false
        self.addButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        self.addButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.addButton.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        self.addButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10.0).isActive = true

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
